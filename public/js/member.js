
(function($) {

$('.editbtn').on('click', function(){
    $('#studenteditmodal').modal('show');

    $tr = $(this).closest('tr');

    var data = $tr.children('td').map(function(){
        return $(this).text();
    }).get();

    console.log(data);

    $('#id').val(data[0]);
    $('#user_id').val(data[1]);
    $('#category_edit').val(data[2]);
    $('#desc_edit').val(data[3]);

});

$('#editFormID').on('submit', function(e){
    e.preventDefault();
    var pattern = /^[a-zA-Z0-9\.\ \,]+$/;
    if(document.getElementById('desc_edit').value == ''){
        swal("Error!", "You must insert the data!", "error");
    }else if(document.getElementById('category_edit').value == 'Choose your category'){
        swal("Error!", "You must choose the category!", "error");
    }else if(!pattern.test($("#desc_edit").val())){
        swal("Error!", "The data must be only letter and number!", "error");
    }
    else{
        var id = $('#id').val();
    $.ajax({
        type: "POST",
        url: "/member/"+id+"/update",
        data: $('#editFormID').serialize(),
        success: function(response){
            console.log(response);
                $('#studenteditmodal').modal('hide');
                swal({
                title: "Congratulation",
                text: "The data has been update",
                icon: "success",
                button: "Ok",
                });
                // window.location.reload();
                window.setTimeout(function(){window.location.reload()}, 3000);
            },
            error: function(error){
                console.log(error);
            }

    });
    }

});
})(jQuery);



$('.delete').click(function(){
    var member_id = $(this).attr('member-id');
    var member_category = $(this).attr('member-category');
    swal({
        title: "Warning",
        text: "Are you sure to delete data with category "+member_category+" !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
           window.location = "/member/"+member_id+"/delete";
        }
        });
});

(function($) {



$('#addFormid').on('submit', function(e){
    e.preventDefault();
    var pattern = /^[a-zA-Z0-9\.\ \,]+$/;
    if(document.getElementById('desc_add').value == ''){
        swal("Error!", "You must insert the data!", "error");
    }else if(document.getElementById('category_add').value == 'Choose your category'){
        swal("Error!", "You must choose the category!", "error");
    }else if(!pattern.test($("#desc_add").val())){
        swal("Error!", "The data must be only letter and number!", "error");
    }else{
        var id = $('#id').val();
    $.ajax({
        type: "POST",
        url: "/member/create",
        data: $('#addFormid').serialize(),
        success: function(response){
            console.log(response);
                $('#studentadd').modal('hide');
                swal({
                title: "Congratulation",
                text: "The data has been added",
                icon: "success",
                button: "Ok",
                });
                // window.location.reload();
                window.setTimeout(function(){window.location.reload()}, 2000);
            },
            error: function(error){
                console.log(error);
            }

    });
    }

});
})(jQuery);

$(document).ready(function(){

fetch_poll_data();

function fetch_poll_data()
{
    $.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


    $.ajax({
        url:"/member/dashboard/fetch",
        method:"POST",
        success:function(data)
        {
            $('#poll_result').html(data);
        }
    });
}


$('#poll_form').on('submit', function(event){
    event.preventDefault();
    var poll_option = '';
    $('.poll_option').each(function(){
        if($(this).prop("checked"))
        {
            poll_option = $(this).val();
        }
    });
    if(poll_option != '')
    {
        // var form = $(this).parents('form.data_form');
        // $('#poll_button').attr('disabled', 'disabled');
        var form_data = $('#poll_form').serialize();
        $.ajax({
            url:"/member/dashboard",
            method:"POST",
            data:form_data,
            success:function()
            {

                $('#poll_form')[0].reset();
                fetch_poll_data();
                swal({
                title: "Congratulation",
                text: "The poll has been update",
                icon: "success",
                button: "Ok",
                });

            }
        });
    }
    else
    {
        swal({
                text: "Please select option",
                icon: "warning",
                button: "Ok",
                });
    }
});

$('.deletepoll').click(function(){
    var poll_id = $(this).attr('poll-id');
    swal({
        title: "Warning",
        text: "Are you sure to down this polling",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
           window.location = "/member/dashboard/"+poll_id+"/deletepoll";
        }
        });
});

});


