(function($) {



    $('#alreadyPOST').on('submit', function(e){
        e.preventDefault();
        var pattern = /^[a-zA-Z0-9\.\ \,]+$/;
        if(document.getElementById('desc').value == ''){
            swal("Error!", "You must insert the data!", "error");
        }else if(document.getElementById('category').value == 'Choose your category'){
            swal("Error!", "You must choose the category!", "error");
        }else if(!pattern.test($("#desc").val())){
            swal("Error!", "The data must be only letter and number!", "error");
        }else{
        $.ajax({
            type: "POST",
            url: "/member/already",
            data: $('#alreadyPOST').serialize(),
            success: function(response){
                console.log(response);
                swal({
                title: "Congratulation",
                text: "The data has been submit",
                icon: "success",
                button: "Ok",
                });
                window.setTimeout(function(){window.location.reload(function(){window.location.href ='/member/dashboard'})}, 2000);

                },
                error: function(error){
                    console.log(error);
                }

        });
        }

    });
    })(jQuery);
