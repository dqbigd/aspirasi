<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{

    protected $fillable = ['member_id','user_id'];
    public function members(){
    	return $this->belongsTo('App\Member');
    }
}
