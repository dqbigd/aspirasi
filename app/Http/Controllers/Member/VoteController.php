<?php

namespace App\Http\Controllers\Member;

use App\Member;
use App\Vote;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    public function data(Request $request)
    {
        $data = $request->get('poll_option');
        $user = Auth::user();
        $member = Member::where('category',$data)->first();
        // $new = new Vote;
        // $new->user_id = $user->id;

        $datapoll = array(
            'user_id' => $user->id,
            'member_id' => $member->id,
             'php_framework'		=>	$data
         );
         Vote::insert($datapoll);
        //  $new->save();

        return redirect('member/dashboard')->with('success','The poll has been submit');

    }

    public function fetch()
  {

        $php_framework = array("Aspirasi", "Laporan", "Kritik & Saran");
        $connect = Vote::all();
        $total_poll_row = $connect->count();
        $output = '';
        if($total_poll_row > 0)
        {
            foreach($php_framework as $row)
            {
                $query = Vote::where('php_framework',$row)->get();
                // $query = "SELECT * FROM tbl_poll WHERE php_framework = '".$row."'";
                // $statement = $connect->prepare($query);
                // $statement->execute();
                $total_row = $query->count();
                $percentage_vote = round(($total_row/$total_poll_row)*100);
                $progress_bar_class = '';
                if($percentage_vote >= 40)
                {
                    $progress_bar_class = 'bg-success';
                }
                else if($percentage_vote >= 25 && $percentage_vote < 40)
                {
                    $progress_bar_class = 'bg-info';
                }
                else if($percentage_vote >= 10 && $percentage_vote < 25)
                {
                    $progress_bar_class = 'bg-warning';
                }
                else
                {
                    $progress_bar_class = 'bg-danger';
                }
                $output .= '
                <div class="row">
                    <div class="col-md-2" align="right">
                        <label>'.$row.'</label>
                    </div>
                    <div class="col-md-10">
                        <div class="progress">
                            <div class="progress-bar '.$progress_bar_class.'" role="progressbar" aria-valuenow="'.$percentage_vote.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$percentage_vote.'%">
                                '.$percentage_vote.' %
                            </div>
                        </div>
                    </div>
                </div>

                ';
            }
        }

        echo $output;

  }


  public function deletepoll($id)
  {

      $poll = Vote::find($id)->delete();

      return redirect('member/dashboard')->with('success','the poll has been update');


  }

  public function coba()
  {
   $vote = Member::with('vote')->get();
   $vote = $vote->pluck('vote');

  dd($vote);
  }

 

}
