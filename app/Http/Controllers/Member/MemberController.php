<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Member;
use App\User;
use App\Vote;
use Session;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{

    public function index()
    {
        $all = null;
        $hello = null;
        $user = Auth::user();
        $userid = $user->id;

        $data = Member::where('user_id',$userid)->first();
        // $already = Member::with('vote')->get();

        if($data){
            $vote = Vote::where('member_id',$data->id)->orWhere('member_id','!=',$data->id)->first();
             $hello = Member::whereNotIn('user_id',[$userid])->first();
            if($hello === null){

                    $member = Member::where('user_id',$userid)->get();
                    return view('web.dashboard')->with(['member'=>$member,'all'=>$all]);

            }else{

                    $all = Member::whereNotIn('user_id',[$userid])->get();
                    $member = Member::where('user_id',$userid)->get();
                    return view('web.dashboard')->with(['member'=>$member,'all'=>$all]);

            }


        }elseif($data === null) {
            $member = Member::where('user_id',$userid)->get();
            $all = Member::all();
            return view('web.dashboard',['member'=>$member,'all'=>$all]);
        }

    }
    public function Register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $user = new User;
        $user->role = 'member';
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $data = User::where('email',$user->email)->get();
        if(!isset($data)){
            return redirect()->back()->with('error','The email already taken');
        }else{
            $password = $request->password;
            $user->password = Hash::make($password);
            $user->save();
            if($user->save())
            {
                Auth::attempt(['email' => $user->email, 'password' => $password]);
                $user = Auth::user();
                $request->session()->put('register','register');
                return redirect('member/already')->with('success','Successfully register');


            }
        }


    // return redirect('/')->with('success','Successfully register');
    }

    public function return()
    {
        return redirect('/');
    }

    public function Login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $email = $request->get('email');
        $password = $request->get('password');
        // $cek =  User::where('role', '=', 'member')->first();

        // $cekEmail = User::where('email',$email)->get();

        if(Auth::attempt(['email' => $email, 'password' => $password])){

                // $user = Auth::user();
                // $all = Member::all();
                // $userid = $user->id;
                // $member = Member::where('user_id',$userid)->get();
                // return view('web.dashboard', ['member'=>$member, 'all'=>$all]);
                $all = null;
                $user = Auth::user();
                $userid = $user->id;

                $data = Member::where('user_id',$userid)->first();
                if($data){
                    $vote = Vote::where('member_id',$data->id)->orWhere('member_id','!=',$data->id)->first();
                    $hello = Member::whereNotIn('user_id',[$userid])->first();
                   if($hello === null){
                       $member = Member::where('user_id',$userid)->get();
                       return redirect('member/dashboard')->with(['member'=>$member,'all'=>$all]);
                   }else{
                       $all = Member::whereNotIn('user_id',[$userid])->get();
                       $member = Member::where('user_id',$userid)->get();
                       return redirect('member/dashboard')->with(['member'=>$member,'all'=>$all]);
                   }

                }elseif($data === null) {
                    $member = Member::where('user_id',$userid)->get();
                    $all = Member::all();
                    return view('web.dashboard',['member'=>$member,'all'=>$all]);
                }
        }
        else{
            return redirect('/')->with('error','Wrong email or password');
        }


        // $passwordhash = Hash::make($password);

        // if (count($email)==1 && count($passwordhash)==1){
        //     $user = Auth::user();
        //     $userid = $user->id;
        //     $member = Member::where('user_id',$userid)->get();
        //     return redirect('web.dashboard',['member'=>$member]);
        // }else{
        //     return redirect('/')->with('fail','Wrong email or password');
        // }


    }

    public function logout()
    {
       $user = User::where('role','member')->get();
        if(!auth()->user()->active){
            Auth::logout();
            return redirect('/')->with('success','logout success');
        }else{
            Auth::user();
        }


    }




    public function addNew(Request $request)
    {
        $user = Auth::user()->getId();
        $member = new Member;
        $member->user_id = $user;
        $member->category = $request->get('category_add');
        $member->desc = $request->get('desc_add');
        $member->save();
        return redirect('member/dashboard')->with('success','New data has been added');
    }


    public function delete($id)
    {
        $member = Member::find($id)->delete();
        return redirect('member/dashboard')->with('success','Data has been delete');
    }


    public function updateList(Request $request, $id)
    {
        $member = Member::find($id);
        $member->category = $request->get('category_edit');
        $member->desc = $request->get('desc_edit');
        $member->save();
        return redirect('member/dashboard')->with('success','Data has been updated');
    }



    public function getProfile()
    {

        return view('web.profile.profile');
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->get('name');
        $user->email = $request->get('email');

        $user->save();
        return redirect('member/profile')->with('success','The profile has been update');
    }


    public function already(Request $request)
    {
        // $user = Auth::user();
        // $userid = $user->id;
        // $member = Member::where('user_id',$userid)->count();

        // if($member > 0){
        //     $all = null;
        //     $data = Member::where('user_id',$userid)->first();
        //         $vote = Vote::where('member_id',$data->id)->orWhere('member_id','!=',$data->id)->first();
        //         if(isset($vote->id)){
        //             $member = Member::where('user_id',$userid)->get();
        //             $all = Member::whereNotIn('user_id',[$userid])->whereNotIn('id',[$vote->id])->get();
        //             return redirect('member/dashboard')->with(['member'=>$member,'all'=>$all]);


        //         }else {
        //             $member = Member::where('user_id',$userid)->get();
        //             $all = Member::all();
        //             return view('member/dashboard',['member'=>$member,'all'=>$all]);
        //         }


        // }
        // else{
        // return view('web.already.already');
        // }

        if($request->session()->has('register')){
            Auth::user();
            return view('web.already.already');
        }else{
            return redirect('/member/dashboard');
        }
    }

    public function alreadypost(Request $request)
    {
        $user = Auth::user()->getId();
        $member = new Member;
        $member->user_id = $user;
        $member->category = $request->get('category');
        $member->desc = $request->get('desc');
        $member->save();
        if($member->save())
        {
            $request->session()->forget('register');
            $all = null;
            $user = Auth::user();
            $userid = $user->id;

            $data = Member::where('user_id',$userid)->first();
            if($data){
                $vote = Vote::where('member_id',$data->id)->orWhere('member_id','!=',$data->id)->first();
                $hello = Member::whereNotIn('user_id',[$userid])->first();
               if($hello === null){
                   $member = Member::where('user_id',$userid)->get();
                   return redirect('member/dashboard')->with(['member'=>$member,'all'=>$all]);
               }else{
                   $all = Member::whereNotIn('user_id',[$userid])->get();
                   $member = Member::where('user_id',$userid)->get();
                   return redirect('member/dashboard')->with(['member'=>$member,'all'=>$all]);
               }

            }elseif($data === null) {
                $member = Member::where('user_id',$userid)->get();
                $all = Member::all();
                return view('web.dashboard',['member'=>$member,'all'=>$all]);
            }
        }


    }

    public function delsesi(Request $request)
    {  if($request->session()->has('register')){
        $request->session()->forget('register');
        return redirect('/member/dashboard');
    }

    }

    public function coba2()
    {
        $member = Member::all();
        $user = Auth::user();
        $member = Member::whereNotIn('user_id',[$user->id])->get();

        $data = Member::with('vote')->get();


        foreach($data as $data)
        {
            $data = $data->member_id;
            print_r($data);
        }

    //    $vote = Vote::where('user_id',$user->id)->count();
    //    if($vote > 0){
    //     $vote = Vote::where('user_id',[$user->id])->get();
    //       foreach($vote as $pote)
    //     {

    //         $hello = $pote->member_id;
    //     }
    //     $member = Member::whereIn('user_id',[$user->id])->whereIn('id',[$hello])->get();



    //    }
        // foreach($vote as $pote)
        // {

        //     $hello = $pote->member_id;
        // }


        // if(!$pote)
        // {
        //     $member = Member::whereNotIn([$user->id],'user_id')->whereNotIn('id',[$hello])->get();

        // }else{
        //      $member = Member::whereNotIn([$user->id],'user_id')->get();
        //      dd($member);
        // }


    }

    public function search(Request $request)
    {
        $user = Auth::user();
        if($request->has('cari')){
            $all = Member::where('user_id','!=',$user->id)->where(function($b) use ($request){
                $b->where('category','LIKE','%'.$request->cari.'%')->orWhere('desc','LIKE','%'.$request->cari.'%');
            })->get();
            $member = Member::where('user_id',$user->id)->where(function($a) use ($request){
                $a->where('category','LIKE','%'.$request->cari.'%')->orWhere('desc','LIKE','%'.$request->cari.'%');
            })->get();
        }

        return view('web.search.search',['member'=>$member,'all'=>$all]);


    }

}
