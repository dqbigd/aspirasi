<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apiBerita($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result,true);
    }

    public function web()
    {
        $url = 'https://newsapi.org/v2/top-headlines?country=id&apiKey=4903e2a8f22f4b7caf1b3ad5b0dbf510';
        $url = $this->apiBerita($url);
        $url = $url['articles'];
        if(Auth::check()){
            return redirect('/member/dashboard');
        }else{
            return view('welcome',['url'=>$url]);
        }



        // foreach($url as $i => $data){
        //    echo '<h2>'.$data['title'].'</h2>';
        //    echo '<img src="' . $data['urlToImage'] . '"/>';
        //    echo '<p>Published on ' . $data['publishedAt'] . '</p>';
        //    echo '<p>' . $data['description'] . '</p>';
        //    echo '<a href="' . $data['url'] . '">Read more</a>';
        // }
    }
}
