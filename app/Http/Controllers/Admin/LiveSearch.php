<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LiveSearch extends Controller

{
    public function index()
    {
        return view('admin.aspirasi.aspirasi');
    }
    public function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = DB::table('members')
         ->where('category', 'like', '%'.$query.'%')
         ->orWhere('desc', 'like', '%'.$query.'%')
         ->orderBy('user_id', 'desc')
         ->get();
         
      }
      else
      {
       $data = DB::table('members')
         ->orderBy('user_id', 'desc')
         ->get();
      }
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        $output .= '
        <tr>
        <th scope="row">'.$row->CustomerName.'</th>
         <td>'.$row->category.'</td>
         <td>'.$row->desc.'</td>
        </tr>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

      echo json_encode($data);
     }
    }
}
