<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Vote;
use Validator;
use App\Member;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
        $member = Member::all()->count();
        return view('admin.dashboard',['member'=>$member]);
    }
    public function getLogin()
    {
        $cekrole = User::where('role','admin')->get();
        $cekrole2 = User::where('role','member')->get();
        if(Auth::check()){
           if($cekrole){
            return redirect('admin/dashboard');
           }elseif($cekrole2){
            return redirect('/');
       }
        }else{
            return view('auths.login');
        }
    }


    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $email = $request->get('email');
        $password = $request->get('password');
        $cekrole = User::where('role','admin')->get();

        if(Auth::attempt(['email' => $email, 'password' => $password])){

                $user = Auth::user();
                $member = Member::all()->count();
                return view('admin/dashboard',['member'=>$member]);


        }else{
            return redirect('/admin')->with('fail','Wrong email or password');
        }


    }

    public function logout()
    {
        $user = User::where('role','admin');
        if($user){
        Auth::logout();
        return redirect('/admin');
        }
    }


    public function aspirasi(Request $request)
    {
        if($request->has('cari')){
            $member = Member::where('category','LIKE','%'.$request->cari.'%')->orWhere('desc','LIKE','%'.$request->cari.'%')->get();
        }else{
        $member = Member::paginate(10);
        }

        $vote = Vote::all();
        return view('admin.aspirasi.aspirasi',['member'=>$member,'vote'=>$vote]);
    }


    public function fetch()
    {

          $php_framework = array("Aspirasi", "Laporan", "Kritik & Saran");
          $connect = Vote::all();
          $total_poll_row = $connect->count();
          $output = '';
          if($total_poll_row > 0)
          {
              foreach($php_framework as $row)
              {
                  $query = Vote::where('php_framework',$row)->get();
                  // $query = "SELECT * FROM tbl_poll WHERE php_framework = '".$row."'";
                  // $statement = $connect->prepare($query);
                  // $statement->execute();
                  $total_row = $query->count();
                  $percentage_vote = round(($total_row/$total_poll_row)*100);
                  $progress_bar_class = '';
                  if($percentage_vote >= 40)
                  {
                      $progress_bar_class = 'bg-success';
                  }
                  else if($percentage_vote >= 25 && $percentage_vote < 40)
                  {
                      $progress_bar_class = 'bg-info';
                  }
                  else if($percentage_vote >= 10 && $percentage_vote < 25)
                  {
                      $progress_bar_class = 'bg-warning';
                  }
                  else
                  {
                      $progress_bar_class = 'bg-danger';
                  }
                  $output .= '
                  <div class="row">
                      <div class="col-md-3" align="right">
                          <label>'.$row.'</label>
                      </div>
                      <div class="col-md-9">
                          <div class="progress">
                              <div class="progress-bar '.$progress_bar_class.'" role="progressbar" aria-valuenow="'.$percentage_vote.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$percentage_vote.'%">
                                  '.$percentage_vote.' %
                              </div>
                          </div>
                      </div>
                  </div>

                  ';
              }

          }

          echo $output;

    }

   
}

