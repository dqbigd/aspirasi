<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ['user_id','category','desc'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function vote(){
    	return $this->hasMany('App\Vote');
    }

}
