
@extends('admin.master')

@section('container')
<div class="row">
    <div class="col-md-8">

               <div class="card-body">
                            <div id="poll_result"></div>

               </div>

        <br>
        <button  id="displayData" class="btn btn-primary">refresh</button>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </div>
</div>
<div class="card mb-3"style="margin-top: 50px;">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Data Aspirasi</div>
        <div class="card-body">
          <div class="table-responsive">
                <table class="table table-hover">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Username</th>
                            <th scope="col">Category</th>
                            <th scope="col">Description</th>
                            <th scope="col">Amount of vote</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($member as $mbr)
                          <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                          <td>{{$mbr->user->name}}</td>
                            <td>{{$mbr->category}}</td>
                            <td>{{$mbr->desc}}</td>
                          <td>{{$mbr->vote->count()}}</td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                      {{$member->links()}}
          </div>
        </div>
      </div>

@endsection

@section('footer')
    <script src="{{asset('js/admin.js')}}"></script>
@endsection
