<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <link rel="icon" type="image/png" href="https://res.cloudinary.com/dbyll18sm/image/upload/v1575172653/favicon_dswx1x.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin | Formasa</title>

  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> --}}
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}
  <!-- Custom fonts for this template-->
  <link href="{{asset('asset/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  {{-- <link href="{{asset('asset/vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet"> --}}

  <!-- Custom styles for this template-->
  <link href="{{asset('asset/css/sb-admin.css')}}" rel="stylesheet">

</head>

<body id="page-top">

  @include('admin.includes._navbar')

  <div id="wrapper">

    <!-- Sidebar -->
    @include('admin.includes._sidebar')

    <div id="content-wrapper">

      <div class="container-fluid">

     @yield('container')


      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Formasa 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{Route('admin.logout')}}">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  {{-- <script src="{{asset('asset/vendor/jquery/jquery.min.js')}}"></script> --}}
  <script src="{{asset('asset/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('asset/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Page level plugin JavaScript-->
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> --}}
  <script src="{{asset('asset/vendor/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('asset/vendor/datatables/jquery.dataTables.js')}}"></script>
  {{-- <script src="{{asset('asset/vendor/datatables/dataTables.bootstrap4.js')}}"></script> --}}

  <!-- Custom scripts for all pages-->
  <script src="{{asset('asset/js/sb-admin.min.js')}}"></script>

  <!-- Demo scripts for this page-->
  <script src="{{asset('asset/js/demo/datatables-demo.js')}}"></script>
  <script src="{{asset('asset/js/demo/chart-area-demo.js')}}"></script>
  @yield('footer')
  {{-- <script>
      $(document).ready(function(){

       fetch_customer_data();

       function fetch_customer_data(query = '')
       {
        $.ajax({
         url:"{{ route('admin.aspirasisearch') }}",
         method:'GET',
         data:{query:query},
         dataType:'json',
         success:function(data)
         {
          $('tbody').html(data.table_data);
         }
        })
       }

       $(document).on('keyup', '#search', function(){
        var query = $(this).val();
        fetch_customer_data(query);
       });
      });
      </script> --}}
</body>

</html>
