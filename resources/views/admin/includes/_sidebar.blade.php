<ul class="sidebar navbar-nav">
    <li class="nav-item {{ Request::path() ==  'admin/dashboard' ? 'active' : 'admin/dashboard'  }} ">
      <a class="nav-link" href="{{Route('admin.dashboard')}}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    
    <li class="nav-item {{ Request::path() ==  'admin/aspirasi' ? 'active' : 'admin/aspirasi'  }}">
      <a class="nav-link" href="{{Route('admin.aspirasi')}}">
        <i class="fas fa-fw fa-list"></i>
        <span>Aspirasi</span></a>
    </li>
  </ul>