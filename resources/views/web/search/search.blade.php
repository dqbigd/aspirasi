
@extends('web.master')

@section('content')

<div class="row" >
    <div class="col-md-12">
        <div class="card">
                @if($all->count() > 0)
                <div class="card-header">
                    <h4 class="card-title">Found {{$all->count()}} Polling system</h4>
                </div>
               <div class="card-body">
                    <div class="scrollable table-responsive">
                            <table class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Category</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Poll</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>

                                    <tbody>
                                            <form id="poll_form" class="data_form">
                                                @csrf
                                                {{method_field('POST')}}
                                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                        @foreach($all as $poll)
                                      <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$poll->category}}</td>
                                        <td>{{$poll->desc}}</td>
                                      <td ><input type="radio" name="poll_option" class="poll_option" value="{{$poll->category}}"/></td>
                                        <td> <button class="btn btn-primary btn-md" name="poll_button" id="poll_button" {{$poll->id}} type="submit">Up</button>
                                        </td>
                                      </tr>

                                      @endforeach
                                    </form>

                                    </tbody>
                                  </table>

                    </div>
               </div>
               @else
               <div class="card-header">
                    <h4 class="card-title">Polling system</h4>
                </div>
                <h4 style="text-align: center;">Not Found</h4>
               @endif
        </div>
    </div>

</div>
<div class="row">

        <div class="col-md-12">

          <div class="card">
                @if($member->count() > 0)
            <div class="card-header">
            <h4 class="card-title">Found {{$member->count()}} Data</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>No</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Action</th>

                </thead>
                    @else
                    <div class="card-header">
                            <h4 class="card-title">Data</h4>
                    </div>
                            <h4 style="text-align: center;">Not Found</h4>


                    @endif

                  <tbody>


                      @foreach ($member as $mbr)


                    <tr>
                    <th>{{$loop->iteration}}</th>
                    <td style="display:none;">{{$mbr->id}}</td>
                    <td style="display:none;">{{$mbr->user_id}}</td>
                      <td>{{$mbr->category}}</td>
                      <td>{{$mbr->desc}}</td>
                      <td >
                            <button class="btn btn-success btn-md editbtn" >Edit</button>
                      <button class="btn btn-danger btn-md delete" member-id={{$mbr->id}} member-category="{{$mbr->category}}">Delete</button>
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


{{-- modal edit data --}}

      </div>
      <div class="modal fade" id="studenteditmodal" tabindex="-1" role="dialog" aria-labelledby="studenteditmodal" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h3 class="modal-title" id="studenteditmodal">Edit Aspirasi</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                <form  id="editFormID" name="editFormID">
                            {{csrf_field()}}
                            {{method_field('POST')}}
                            <input type="hidden" name="id" id="id">
                            {{-- <input type="hidden" name="user_id"> --}}
                                <div class="form-group">
                                  <label for="category">Category</label>
                                  <select id="category_edit" name="category_edit" required class="form-control">
                                        <option value="Choose your category">Choose your category</option>
                                        <option value="Aspirasi">Aspirasi</option>
                                        <option value="Laporan">Laporan</option>
                                        <option value="Kritik & Saran">Kritik & Saran</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                  <label for="desc">Description</label>
                                  <textarea id="desc_edit" name="desc_edit" class="form-control"  placeholder="Write your reason in here"> </textarea>
                                </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
                </div>
            </div>
            </div>
        </div>




  <!-- Modal -->



@endsection
@section('footer')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('js/membersearch.js')}}"></script>
@stop
