<div class="modal fade login" id="loginModal">
        <div class="modal-dialog login animated modal-dialog-centered">
            <div class="modal-content">
               <div class="modal-header">
                  {{-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> --}}
                 <h4 class="modal-title" style="margin: 0 auto;">Login</h4>
              </div>
              <div class="modal-body">
                  <div class="box">
                       <div class="content">

                          <div class="form loginBox">
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                </div>
                                @endif
                                @if(session('fail'))
                                <div class="alert alert-danger  " role="alert">
                                       {{session('fail')}}
                                    </div>
                                    @endif
                              <form method="POST" action="{{Route('memberlogin')}}" html="{:multipart=>true}" data-remote="true" accept-charset="UTF-8">
                                {{ csrf_field() }}
                              <input id="email" class="form-control" type="text" placeholder="Email" name="email" required>
                              <input id="password" class="form-control" type="password" placeholder="Password" name="password" required>
                              <button type="submit" class="btn btn-default btn-login">Login</button>
                              </form>
                          </div>
                       </div>
                  </div>
                  <div class="box">
                    <div class="content registerBox" style="display:none;">
                     <div class="form">
                            @if($errors->any())
                            <div class="alert alert-danger" role="alert">
                                {{$errors->first()}}
                            </div>
                            @endif
                        <form method="POST" html="{:multipart=>true}" data-remote="true" action="{{Route('memberregister')}}" accept-charset="UTF-8">
                        {{ csrf_field() }}
                        <input id="name" class="form-control" type="text" placeholder="Your Name" name="name" required>
                        <input id="email" class="form-control" type="text" placeholder="Email" name="email" required>
                        <input id="password" class="form-control" type="password" placeholder="Password" name="password" required>
                        <input id="password_confirmation" class="form-control" type="password" placeholder="Repeat Password" name="c_password" required>
                        <button type="submit2" class="btn btn-default btn-register">Create account</button>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                        <div class="content forgotBox" style="display:none;">
                         <div class="form">
                                @if($errors->any())
                                <div class="alert alert-danger" role="alert">
                                    {{$errors->first()}}
                                </div>
                                @endif
                            <form method="POST" html="{:multipart=>true}" data-remote="true" action="{{Route('password.email')}}" accept-charset="UTF-8">
                            {{ csrf_field() }}
                            <input id="email" class="form-control" type="text" placeholder="Email" name="email" required>
                            <button type="submit3" class="btn btn-default btn-forgot">Send</button>
                            </form>
                            </div>
                        </div>
                    </div>

              </div>
              <div class="modal-footer">
                <div class="forgot">
                     <span class="register-footer" style="display:none">Already have an account?
                        <a href="javascript:showLoginForm();">Login</a></span>
                        <br>
                    <span class="login-footer">Looking to
                         <a href="javascript:showRegisterForm();">create an account ?</a>
                    </span>
                    <br>

                <span class="forgot-footer"><a href="javascript:showForgotForm();" >Forgot Password</a></span>

                </div>

            </div>
            </div>
        </div>
    </div>

