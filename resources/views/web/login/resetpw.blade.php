@extends('web.login.main')

@section('content')
<div class="wrapper fadeInDown">
        <div id="formContent">
          <!-- Tabs Titles -->

          <!-- Icon -->
          <div class="fadeIn first">
            <h4 class="modal-title" style="margin: 0 auto;">Reset Password</h4>
          </div>

          <!-- Login Form -->
        <form method="POST" action="{{Route('password.update')}}">
            @csrf
            @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
					@endif
            <input type="hidden" name="token" value="{{ $token }}">
            <input type="text" class="fadeIn second @error('email') is-invalid @enderror" name="email"value="{{ $email ?? old('email') }}" placeholder="Email" required>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong style="color: red;">{{ $message }}</strong>
            </span>
        @enderror
            <input type="password" class="fadeIn second" name="password" placeholder="New Password" required>
            <input type="password" class="fadeIn second" name="password_confirmation" placeholder="Password Confirmation" required>
            <button type="submit"  class="fadeIn fourth">Change</button>
          </form>


        </div>
      </div>
@endsection
