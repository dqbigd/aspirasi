@extends('web.already.main')

@section('content')

<div class="row">
    <div class="col-md-2">

    </div>
    <div class="col-md-8">
        <div class="card">

            <div class="card-header">
                <h5 class="card-title">Hello {{Auth()->user()->name}},</h5>
            </div>
            <div class="card-body">
            <form  id="alreadyPOST" >
                    @csrf
                    {{method_field('POST')}}
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                        <label for="category">Category</label>
                                        <select id="category" name="category" required class="form-control">
                                              <option value="Choose your category">Choose your category</option>
                                              <option value="Aspirasi">Aspirasi</option>
                                              <option value="Laporan">Laporan</option>
                                              <option value="Kritik & Saran">Kritik & Saran</option>
                                          </select>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                        <label for="desc">Description</label>
                                        <textarea id="desc" name="desc"  rows="4" cols="80"class="form-control"  placeholder="Write your reason in here"></textarea>
                                </div>
                              </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-4">

                                </div>

                                <div class="col-md-4" style="text-align:center;">
                                        <button type="submit" class="btn btn-primary" >submit</button>
                                </div>
                                <div class="col-md-4" style="text-align:right;">  <button type="button" class="btn btn-success" onclick="window.location.href='{{route('member.delsesi')}}'" value="Go to Google" >skip </button>

                                </div>
                            </div>



                          </form>
            </div>
        </div>
    </div>
    <div class="col-md-2">

    </div>
</div>

@endsection
@section('footer')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('web/js/already.js')}}"></script>
@endsection
