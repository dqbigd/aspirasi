
@extends('web.master')

@section('content')

<div class="row" >
    <div class="col-md-6">
        <div class="card">
                <div class="card-header">
                        <h4 class="card-title">Polling system</h4>
                      </div>
               <div class="card-body">
                    <div class="scrollable table-responsive">
                            <table class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Category</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Poll</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                            <form id="poll_form" class="data_form">
                                                @csrf
                                                {{method_field('POST')}}
                                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                        @if($all == null)

                                        @else
                                        @foreach($all as $poll)


                                      <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$poll->category}}</td>
                                        <td>{{$poll->desc}}</td>
                                      <td class="text-center"><input type="radio" name="poll_option" class="poll_option" value="{{$poll->category}}"/></td>
                                        <td> <button class="btn btn-primary btn-md" name="poll_button" id="poll_button" {{$poll->id}} type="submit">Up</button>

                                          {{-- @foreach($poll->vote as $id)
                                            <button class="btn btn-danger btn-md deletepoll"  type="button" poll-id="{{$id->id}}">Down</button>
                                          @endforeach --}}
                                        </td>
                                      </tr>

                                      @endforeach
                                      @endif
                                    </form>

                                    </tbody>
                                  </table>
                    </div>
               </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="card">
                    <div class="card-header">
                            <h4 class="card-title">Rank polling</h4>
                          </div>
                   <div class="card-body">
                        <div class="scrollable table-responsive">
                                <div id="poll_result"></div>
                        </div>

                   </div>
            </div>
        </div>
</div>
<div class="row">

        <div class="col-md-12">

          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Data</h4>
            </div>
            <div class="col-sm-12 float-right">
            <button class="btn btn-primary addata" data-toggle="modal" data-target="#studentadd">Add Data</button>
                </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>No</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Action</th>
                  </thead>
                  <tbody>


                      @foreach ($member as $mbr)


                    <tr>
                    <th>{{$loop->iteration}}</th>
                    <td style="display:none;">{{$mbr->id}}</td>
                    <td style="display:none;">{{$mbr->user_id}}</td>
                      <td>{{$mbr->category}}</td>
                      <td>{{$mbr->desc}}</td>
                      <td >
                            <button class="btn btn-success btn-md editbtn" >Edit</button>
                      <button class="btn btn-danger btn-md delete" member-id={{$mbr->id}} member-category="{{$mbr->category}}">Delete</button>
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


{{-- modal edit data --}}

      </div>
      <div class="modal fade" id="studenteditmodal" tabindex="-1" role="dialog" aria-labelledby="studenteditmodal" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h3 class="modal-title" id="studenteditmodal">Edit Aspirasi</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                <form  id="editFormID" name="editFormID">
                            {{csrf_field()}}
                            {{method_field('POST')}}
                            <input type="hidden" name="id" id="id">
                            {{-- <input type="hidden" name="user_id"> --}}
                                <div class="form-group">
                                  <label for="category">Category</label>
                                  <select id="category_edit" name="category_edit" required class="form-control">
                                        <option value="Choose your category">Choose your category</option>
                                        <option value="Aspirasi">Aspirasi</option>
                                        <option value="Laporan">Laporan</option>
                                        <option value="Kritik & Saran">Kritik & Saran</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                  <label for="desc">Description</label>
                                  <textarea id="desc_edit" name="desc_edit" class="form-control"  placeholder="Write your reason in here"> </textarea>
                                </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
                </div>
            </div>
            </div>
        </div>




  <!-- Modal -->
  <div class="modal fade" id="studentadd" tabindex="-1" role="dialog" aria-labelledby="studentaddLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="studentaddLabel">Add data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form  name="addFormid" id="addFormid">
          {{ csrf_field() }}
          {{method_field('POST')}}
        <div class="modal-body">
            <div class="form-group">
                <label for="category">Category</label>
                <select id="category_add" name="category_add" required class="form-control">
                      <option value="Choose your category">Choose your category</option>
                      <option value="Aspirasi">Aspirasi</option>
                      <option value="Laporan">Laporan</option>
                      <option value="Kritik & Saran">Kritik & Saran</option>
                  </select>

              </div>
              <div class="form-group">
                <label for="desc">Description</label>
                <textarea id="desc_add" name="desc_add" class="form-control" required placeholder="Write your reason in here"></textarea>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>


@endsection
@section('footer')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('js/member.js')}}"></script>
@stop
