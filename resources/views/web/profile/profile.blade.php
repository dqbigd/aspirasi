@extends('web.master')

@section('content')
<div class="row">

    <div class="col-md-4">
        <div class="card card-user">
          <div class="image">
            <img src="{{asset('member/img/bg5.jpg')}}" alt="...">
          </div>
          <div class="card-body">
            <div class="author">
              <a href="#">
                <img class="avatar border-gray" src="{{asset('member/img/mike.jpg')}}" alt="...">
                <h5 class="title">{{Auth()->user()->name}}</h5>
              </a>

            </div>
            <p class="description text-center">
              {{Auth()->user()->email}}
            </p>
          </div>
          <hr>
          <div class="button-container">
            <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">

            </button>
            <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">

            </button>
            <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">

            </button>
          </div>
        </div>
      </div>

      <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Edit Profile</h5>
              </div>
              <div class="card-body">
                <form  id="POSTEDIT">
                    @csrf
                    {{method_field('POST')}}
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" required name="name"  placeholder="Name" value="{{Auth()->user()->name    }}">
                      </div>
                    </div>
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" required name="email" placeholder="Email" value="{{Auth()->user()->email}}">
                      </div>
                    </div>

                  </div>

                  <button type="submit" class="btn btn-primary">Save</button>
                </form>
              </div>
            </div>
          </div>

</div>
@endsection

@section('footer')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
$('#POSTEDIT').on('submit', function (e) {
  e.preventDefault();

  Swal.fire({
  title: 'Warning',
  text: "Are you sure to update it ?",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, update it!'
}).then((result) => {
  if (result.value) {
    $.ajax({
        type: "POST",
        url: "{{Route('member.upprofile')}}",
        data: $('#POSTEDIT').serialize(),
        success: function(response){
            console.log(response);
            Swal.fire(
                'Update!',
                'Your profile has been update.',
                'success'
                )
                window.setTimeout(function(){window.location.reload()}, 1000);
            },
            error: function(error){
                console.log(error);
            }

    });
  }
})
});
</script>
@endsection
