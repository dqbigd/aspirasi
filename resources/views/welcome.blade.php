@extends('web.main2')

@section('main')

      <!-- End Section Tabs -->
      <div class="section" id="carousel">
            <div class="container">
              <div class="title text-center">
                <h2>Berita Terpopuler</h2>
              </div>
              <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12">
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="3" class=""></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="4" class=""></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="5" class=""></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="6" class=""></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="7" class=""></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="8" class=""></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        {{ $isFirst = true}}
                        @foreach ($url as $item=>$data)
                        <div class="carousel-item {{{ $isFirst ? ' active' : '' }}}">
                                <img class="d-block" src="{{$data['urlToImage']}}" alt="First slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <div class="card">
                                            <h5 style="color: black;">{{$data['title']}}</h5>
                                    </div>

                                </div>
                        </div>
                         {{ $isFirst = false}}
                        @endforeach


                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <i class="now-ui-icons arrows-1_minimal-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <i class="now-ui-icons arrows-1_minimal-right"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

      <div class="section" id="carousel">
        <div class="container">
          <div class="title" style="text-align:center;">
            <h2>Menu Formasa</h2>
          </div>
          <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img class="d-block" src="{{asset('web/assets/img/bg1.jpg')}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                      <h5 style="color: black;" >Dashboard</h5>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block" src="{{asset('web/assets/img/bg3.jpg')}}" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                      <h5 style="color: black;">Add data</h5>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block" src="{{asset('web/assets/img/bg4.jpg')}}" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <h5 style="color: black;">Delete Data</h5>
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>


        <div class="section section-download" id="#download-section" data-background-color="black">
        <div class="container">

          <br>
          <br>
          <br>
          <div class="row text-center mt-5">
            <div class="col-md-8 ml-auto mr-auto">
              {{-- <h2>Want more?</h2> --}}
              <h5 class="description">We've just launched
                Formasa. It has a polling system that you can give a reason to the goverment.</h5>
            </div>

          </div>
          <br>
          <br>
          <div class="row justify-content-md-center sharing-area text-center">
            <div class="text-center col-md-12 col-lg-8">
              <h3>Thank you for supporting us!</h3>
            </div>

          </div>
        </div>
      </div>




@endsection
