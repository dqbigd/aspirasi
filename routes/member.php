<?php
Route::post('/loginmember', array('uses'=>'Member\MemberController@Login'))->name('memberlogin');

Route::post('/register', array('uses'=>'Member\MemberController@Register'))->name('memberregister');
Route::get('password/reset/','Member\ForgotPwController@showLinkRequestForm')->name('password.request');
Route::post('password/email',array('uses'=>'Member\ForgotPwController@sendResetLinkEmail'))->name('password.email');
Route::get('password/reset/{token}',array('uses'=>'Member\ResetPwController@showResetForm'))->name('password.reset');
Route::post('password/reset', 'Member\ResetPwController@reset')->name('password.update');
Route::name('member.')->group(function(){
    Route::group([

        'namespace'     => 'Member',
        'prefix'        => 'member',
        'middleware'    => 'auth'

    ], function () {
        Route::get('/dashboard/search','MemberController@search')->name('search');
        // Route::get('/coba','MemberController@coba2')->name('coba');
        Route::get('/already','MemberController@already')->name('already');
        Route::post('/already','MemberController@alreadypost')->name('alreadypost');
        Route::get('/logout','MemberController@logout')->name('logout');
        Route::get('/profile','MemberController@getProfile')->name('profile');
        Route::get('/dashboard', 'MemberController@index')->name('dashboard');
        Route::get('/{id}/delete','MemberController@delete');
        Route::post('/{id}/update','MemberController@updateList');
        Route::post('/create','MemberController@addNew')->name('addNew');
        Route::post('/update','MemberController@updateProfile')->name('upprofile');
        Route::post('/dashboard','VoteController@data')->name('data');
        Route::get('/dashboard/{id}/deletepoll','VoteController@deletepoll');
        Route::get('/session','MemberController@delsesi')->name('delsesi');
        Route::post('/dashboard/fetch','VoteController@fetch')->name('fetch');
        // Route::get('/coba','VoteController@coba');


        // Route::name('aspirasi')->group(function(){
        //     Route::prefix('aspirasi')->group(function(){
        //        Route::view('/','member.aspirasi.aspirasi');
        //     });
        // });

    });
});
