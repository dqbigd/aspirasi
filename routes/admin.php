<?php
Route::get('/admin', array('uses'=>'Admin\AdminController@getLogin'));
Route::post('/admin', array('uses'=>'Admin\AdminController@postLogin'))->name('postLogin');

Route::name('admin.')->group(function(){
    Route::group([

        'namespace'     => 'Admin',
        'prefix'        => 'admin',
        'middleware'    => ['auth','checkrole:admin']

    ], function () {
        // Route::get('/profile','AdminController@editprofile')->name('editadmin');
        Route::get('/logout','AdminController@logout')->name('logout');
        // Route::view('/dashboard', 'admin.dashboard')->name('dashboard');
        Route::get('/dashboard','AdminController@index')->name('dashboard');


        Route::post('/fetch','AdminController@fetch')->name('fetch');
        Route::name('aspirasi')->group(function(){
            Route::prefix('aspirasi')->group(function(){
                // Route::get('/search','LiveSearch@action')->name('search');
              Route::get('/','AdminController@aspirasi');

            });
        });

    });
});
