<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/api','Web\WebController@berita');
Route::get('/webku','Web\WebController@web');
Route::get('/','Web\WebController@web')->name('login');


require('admin.php');
require('member.php');
